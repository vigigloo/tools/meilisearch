resource "helm_release" "meilisearch" {
  chart           = "meilisearch"
  repository      = "https://gitlab.com/api/v4/projects/32747266/packages/helm/stable"
  name            = var.chart_name
  namespace       = var.namespace
  version         = var.chart_version
  force_update    = var.helm_force_update
  recreate_pods   = var.helm_recreate_pods
  cleanup_on_fail = var.helm_cleanup_on_fail
  max_history     = var.helm_max_history

  values = var.values

  dynamic "set" {
    for_each = var.image_repository == null ? [] : [var.image_repository]
    content {
      name  = "image.repository"
      value = var.image_repository
    }
  }

  dynamic "set" {
    for_each = var.replicaCount == null ? [] : [var.replicaCount]
    content {
      name  = "replicaCount"
      value = var.replicaCount
    }
  }

  dynamic "set" {
    for_each = var.persistence == null ? [] : [var.persistence]
    content {
      name  = "persistence.enabled"
      value = var.persistence
    }
  }

  dynamic "set" {
    for_each = var.meilisearch_environment == null ? [] : [var.meilisearch_environment]
    content {
      name  = "environment.MEILI_ENV"
      value = var.meilisearch_environment
    }
  }

  dynamic "set" {
    for_each = var.meilisearch_master_key_secret == null ? [] : [var.meilisearch_master_key_secret]
    content {
      name  = "auth.existingMasterKeySecret"
      value = var.meilisearch_master_key_secret
    }
  }

  dynamic "set" {
    for_each = var.meilisearch_analytics == null ? [] : [var.meilisearch_analytics]
    content {
      name  = "environment.MEILI_NO_ANALYTICS"
      value = var.meilisearch_analytics ? "false" : "true"
    }
  }

  dynamic "set" {
    for_each = var.image_tag == null ? [] : [var.image_tag]
    content {
      name  = "image.tag"
      value = var.image_tag
    }
  }

  dynamic "set" {
    for_each = var.limits_cpu == null ? [] : [var.limits_cpu]
    content {
      name  = "resources.limits.cpu"
      value = var.limits_cpu
    }
  }

  dynamic "set" {
    for_each = var.limits_memory == null ? [] : [var.limits_memory]
    content {
      name  = "resources.limits.memory"
      value = var.limits_memory
    }
  }

  dynamic "set" {
    for_each = var.requests_cpu == null ? [] : [var.requests_cpu]
    content {
      name  = "resources.requests.cpu"
      value = var.requests_cpu
    }
  }

  dynamic "set" {
    for_each = var.requests_memory == null ? [] : [var.requests_memory]
    content {
      name  = "resources.requests.memory"
      value = var.requests_memory
    }
  }

  dynamic "set" {
    for_each = var.ingress_host == null ? [] : [var.ingress_host]
    content {
      name  = "ingress.host"
      value = var.ingress_host
    }
  }
}
