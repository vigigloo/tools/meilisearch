variable "meilisearch_environment" {
  type    = string
  default = "production"
}

variable "meilisearch_master_key_secret" {
  type    = string
  default = null
}

variable "meilisearch_analytics" {
  type    = bool
  default = false
}

variable "persistence" {
  type    = bool
  default = false
}