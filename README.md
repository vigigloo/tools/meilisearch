# Meilisearch

## Backup using dumps

### Creating a dump

[Official documentation to create a dump](https://docs.meilisearch.com/learn/advanced/dumps.html#creating-a-dump)

First, use port-forwarding to easily access the API:
```bash
kubectl port-forward meilisearch-0 7700
```

Trigger the API endpoint for triggering a dump.
Look for the UID in the answer.

```bash
export ADMIN_TOKEN="xxxxx"
curl -X POST -H "Authorization: Bearer $ADMIN_TOKEN" 'http://localhost:7700/dumps'
# {"uid":"20220603-070644772","status":"in_progress","startedAt":"2022-06-03T07:06:44.772653842Z"}
```

Check the status of the dump using the previous UID.

```bash
export DUMP_UID="20220603-070644772"
curl -X GET -H "Authorization: Bearer $ADMIN_TOKEN" "http://localhost:7700/dumps/$DUMP_UID/status"
# {"uid":"20220603-070644772","status":"done","startedAt":"2022-06-03T07:06:44.772653842Z","finishedAt":"2022-06-03T07:06:44.824238237Z"}
```

If the dump is finished, copy it to wherever you want.

```bash
kubectl cp "meilisearch-0:/dumps/$DUMP_UID.dump" "$DUMP_UID.dump"
```

### Restoring a dump

[Official documentation to import a dump](https://docs.meilisearch.com/learn/advanced/dumps.html#importing-a-dump)

Fist, you'll need to stop the meilisearch service while still keeping the pod running.

```bash
kubectl patch sts meilisearch --type='json' -p='[{"op": "replace", "path": "/spec/template/spec/containers/0/command", "value": ["sleep", "infinity"]}]'
kubectl rollout restart sts meilisearch
# If the pod still doesn't seem to be renewed
kubectl delete pod meilisearch-0
```

We'll now copy the dump to the pod's filesystem and launch the service from that dump to restore it.
This actually starts the service, so after a while, when it looks like it has loaded the dump correctly you can abort the command.

```bash
export DUMP_UID="20220603-070644772"
kubectl exec meilisearch-0 -- mkdir -p /dumps
kubectl cp "$DUMP_UID.dump" "meilisearch-0:/dumps/$DUMP_UID.dump"
kubectl exec meilisearch-0 -- ./meilisearch --import-dump "/dumps/$DUMP_UID.dump"
# When meilisearch seems to have started after importing the dump you can close it I guess
```

Start the service again by removing its sleep command.

```bash
kubectl patch sts meilisearch --type='json' -p='[{"op": "remove", "path": "/spec/template/spec/containers/0/command"}]'
kubectl rollout restart sts meilisearch
# If the pod still doesn't seem to be renewed
kubectl delete pod meilisearch-0
```
